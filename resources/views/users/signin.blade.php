@extends('layouts.master')
@section('style')
    <link rel="stylesheet" href="{{URL::to('css/app.css')}}"></link>
@endsection
@section('title')
    Shopping | Signin
@endsection
@section('content')
    <div class="gap3"></div>
    <div class="row jumbotron justify-content-center align-items-center">
        <div class=" justify-content-center align-items-center" style="width: 50%; border:1px solid #e6e3e3; background:#f5f5f4;">
            <div class="row" style="padding-left: 16px; margin-top:14px;">
                <div class="col-12" style="text-align:center;">
                    <h3 style="color:#b56969">Login</h3>				
                </div>
            </div>
            <hr style="    width: 50%;">
           <form name="userSignin" method="POST" action="/yourhome/public/signin" class="">
            {{csrf_field()}}
                <div class="row col-md-12" style="margin-left:1px;">
                    <div class="col-md-12 form-group">
                        <label>Email</label>
                        <input  type="email" id="email" ng-model="userData.email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" name="email" class="form-control" required>
                        <span class='text-danger' ng-show="userSignin.email.$dirty&&userSignup.email.$error.pattern">Please Enter Valid Email</span>
                    </div>
                </div>
                <div class="row col-md-12" style="margin-left:1px;">
                    <div class="col-md-12 form-group">
                        <label>Password</label>
                        <input type="password" id="password" minlength="4"  maxlength="8" ng-model="userData.password" name="password" required class="form-control">
                     </div>		
                </div>
                <div class="row" style="text-align:center;">
                    <div class="col-12 form-group">
                        <button type="submit" class="btn  btn-txt-uppercase" style="">Login</button>
                        <a href="" style="color:#22264b;">Forgot your password?</a>					
                    </div>
                </div>
            </form>
        </div>
    </div>
     
</div>
@endsection

    