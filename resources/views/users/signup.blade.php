@extends('layouts.master')
@section('style')
    <link rel="stylesheet" href="{{URL::to('css/app.css')}}"></link>
@endsection
@section('title')
    Shopping | Signup
@endsection
@section('content')

<div class="row form-group" ng-app="signupApp" ng-controller="signupCtrl">
<h1 class="col-md-8  col-md-offset-2">Registration Form</h1>
	<div class="row">
        <form name="userSignup" method="POST" action="/yourhome/public/registerUser" >
        {{csrf_field()}}
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>First Name</label>
                        <input type="text" id="firstName" ng-model="userData.firstName" ng-change="validateIndividualFields('firstName');" name="firstName" class="form-control" required>
                        <span class='text-danger' ng-if="showErrorFirstNameMsg">@{{ message }}</span>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Last Name</label>
                        <input type="text" id="lastName" ng-model="userData.lastName" ng-change="validateIndividualFields('lastName');" name="lastName" class="form-control" required>
                        <span class='text-danger' ng-if="showErrorLastNameMsg">@{{ message }}</span>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-md-6 form-group">
                        <label>Email</label>
                        <input type="email" id="email" ng-model="userData.email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" name="email" class="form-control" required>
                        <span class='text-danger' ng-show="userSignup.email.$dirty&&userSignup.email.$error.pattern">Please Enter Valid Email</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>Password</label>
                        <input type="password" id="password" minlength="4"  maxlength="8" ng-model="userData.password" name="password" required class="form-control">
                         <i class="fa fa-check pull-right" ng-if="matchPassword == true" style="color:rgba(16, 156, 4, 0.76);" aria-hidden="true"></i>
                        <i class="fa fa-times pull-right" ng-if="matchPassword == false" style="color:rgb(231, 5, 5);" aria-hidden="true"></i>
                    </div>	
                    <div class="col-md-6 form-group">
                        <label>Confirm password</label>
                        <input type="password" id="confirmPassword" ng-model="userData.confirmPassword" ng-change="checkConfirmPassword($event);" name="confirmPassword" class="form-control pull-left">
                        <i class="fa fa-check pull-right" ng-if="matchPassword ==true" style="color:rgba(16, 156, 4, 0.76);" aria-hidden="true"></i>
                        <i class="fa fa-times pull-right" ng-if="matchPassword == false" style="color:rgb(231, 5, 5);" aria-hidden="true"></i>
                    </div>	
                </div>
                <div class="row">					
                    <div class="col-md-6 form-group ">
                        <label>Address</label>
                        <textarea placeholder="Enter Address Here.." rows="3" ng-model="userData.address" ng-change="validateIndividualFields('address');" class="form-control" id="address" name="address" required></textarea>
                        <span class='text-danger' ng-if="showErrorAddressMsg">@{{ message }}</span>
                    </div>
                </div>
                <div class="row">    	
                    <div class="col-md-6 form-group">
                        <label>Phone Number</label>
                        <input type="text" ng-model="userData.phoneNumber" id="phoneNumber" name="phoneNumber" class="form-control" required>
                    </div>
                
                    <div class="col-md-6 form-group">
                        <label>City</label>
                        <input type="text" id="city" name="city" ng-model="userData.city" class="form-control">
                    </div>	
                    <div class="col-md-6 form-group">
                        <label>State</label>
                        <input type="text" id="state" name="state" ng-model="userData.state" class="form-control">
                    </div>	
                    <div class="col-md-6 form-group">
                        <label>Zip</label>
                        <input type="text" id="zip" name="zip" ng-model="userData.zip" ng-change="validateIndividualFields('zip');" required class="form-control">
                        <span class='text-danger' ng-if="showErrorZipMsg">@{{ message }}</span>
                    </div>		
                </div>
                <div class="row">
                    <div class="col-md-8 form-group">
                        <hr>
                        <input type="checkbox" required name="terms" ng-model="userData.termsAndCondition"
                        ng-change="validateIndividualFields('termsAndCondition');" id="termsAndCondition">   
                        <label for="terms">I agree with the <a href="terms.php"
                        title="You may read our terms and conditions by clicking on this link">terms and conditions</a> for Registration.</label><span class="req">* </span>
                        <span class='text-danger' ng-if="showErrorTncMsg == ''">@{{ message }}</span>
                    </div>	
                </div>				
            <!--<button type="button" class="btn btn-lg btn-info" ng-click="fnValidateData()">Submit</button>-->
                 <button type="submit" class="btn btn-primary btn-txt-uppercase">Create an account</button>					
            </div>
        </form>
</div>
@endsection
@section('scripts')
    <script src="{{URL::to('js/custom_scripts/signup_signin_scripts/signup_form_validation.js')}}"></script>
    <script src="{{URL::to('js/angular-resource/angular-resource.min.js')}}"></script>
@endsection