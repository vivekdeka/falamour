<?php
 // print_r($categories);
?>
<div style="height: 104px;  margin:0; border:1px soild red;" class=" top-header navbar navbar-expand-lg navbar-fixed-top">
  <div class="fixed-top full-header">
  <div class="row">
    <div class="col-md-4">
      <span class="top-left">
        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> : info@falamour.com | 
        <i class="fa fa-phone" aria-hidden="true"></i> : +002.568.745.890</p>
      </span>
    </div>
    <div class="col-md-4">
      <div class="main-header">
        <div class="logo">
          <h1 class="logo">FALAMOUR</h1>
          <hr style="margin-top: 6px;background: #e6cf8b;">
        </div>
      </div>
    </div>
    <div class="col-md-4 top-right">
        <div class="bag-login-logout">
          <ul>
            <li><a href="/falamour/public/shopping-cart"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Bag</a></li>
            <li><a href="/falamour/public/signin">Login</a></li>
            <li><a href="/falamour/public/signup">Register</a></li>
          </ul>
        </div>
    </div>
    
  </div>

    <nav class="navbar navbar-expand-lg navbar-dark nav-bar-custom-design">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
        <ul class="navbar-nav" id="navMenu">
          <li class="nav-item " id="home">
            <a class="nav-link" href="/falamour/public/">Home</a>
          </li>
          <li class="nav-item " id="newArrivels">
            <a class="nav-link" href="/falamour/public/product-list">New Arrivels</a>
          </li>
          <!--<li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>-->
          <li class="nav-item dropdown" id="menFashion">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown08" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Men's Fashion</a>
            <div class="dropdown-menu category-dropdown" aria-labelledby="dropdown08">
              <a class="dropdown-item" href="/falamour/public/product-list">Clothing</a>
              <a class="dropdown-item" href="/falamour/public/product-list">Footware</a>
              <a class="dropdown-item" href="/falamour/public/product-list">Watches</a>
            </div>
          </li>
           <li class="nav-item dropdown" id="womenFashion">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown08" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Women's Fashion</a>
            <div class="dropdown-menu category-dropdown" aria-labelledby="dropdown08">
              <a class="dropdown-item" href="/falamour/public/product-list">Ethnic wear</a>
              <a class="dropdown-item" href="/falamour/public/product-list">Western wear</a>
              <a class="dropdown-item" href="/falamour/public/product-list">Footwear</a>
            </div>
          </li>
           <li class="nav-item" id="aboutUs">
            <a class="nav-link" href="/falamour/public/about-us">About Us</a>
          </li>
        </ul>
      </div>
    </nav>
    </div>
  </div>