<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{URL::to('css/bootstrap/bootstrap.css')}}"></link>
    <link rel="stylesheet" href="{{URL::to('fontawesome/css/font-awesome.css')}}"></link>
    <link rel="stylesheet" href="{{URL::to('css/app.css')}}"></link>
    @yield('style')
    <title>@yield('title')</title>
    
</head>
<body>
    @include('partials.header')
    @yield('langing-image')
    <div class="container">
        @yield('breadcrumb')
        @yield('content')
    </div>
    <footer>
        @yield('footer')
    </footer>
    
    <script src="{{URL::to('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{URL::to('js/poper.js')}}"></script>
    <script src="{{URL::to('js/angular.min.js')}}"></script>
    <script src="{{URL::to('js/bootstrap/ui-bootstrap-tpls-2.5.0.js')}}"></script>
    <script src="{{URL::to('js/bootstrap/bootstrap.js')}}"></script>
    <script src="{{URL::to('js/custom_scripts/signup_signin_scripts/sign-in-popup.js')}}"></script>
    <script src="{{URL::to('js/features.js')}}"></script>
    
    @yield('scripts')

</body>
</html>