@extends('layouts.master')
@section('title')
    Shopping
@endsection
@section('style')
    <link rel="stylesheet" href="{{URL::to('css/product-style.css')}}"></link>
@endsection
@section('content')
    <div class="row jumbotron">
        <div class="col-6">
            <div class="row">
                 <div class="col-md-4">
                    <ul style="list-style: none;">
                        <li><img src="{{URL::to($product->imagePath)}}" alt="..." class="img-responsive single-product"></li>
                        <li><img src="{{URL::to($product->imagePath)}}" alt="..." class="img-responsive single-product"></li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <img src="{{URL::to($product->imagePath)}}" alt="..." class="img-responsive single-product">
                </div>
            </div>
        </div>
        <div class="col-6" style="text-align:center;">
            <h3>{{$product->title}}</h3>
            <div class="price">Rs. {{$product->price}}</div>
             <div class="img-separator"></div>
             <a href="/yourhome/public/add-to-cart/{{ $product->id }}" style="margin-top: 3%;
    margin-bottom: 3%;" class="btn btn-info shopThis" role="button">add to shopping bag</a>
            <div class="img-separator"></div>
            <p class="description">{{$product->description}}</p>
        </div>
    </div>

     <div class="row">
         <h4>YOU MAY ALSO LIKE</h4>
         <div class="img-separator"></div>
           
     </div>

@endsection