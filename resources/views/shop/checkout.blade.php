@extends('layouts.master')
@section('title')
    Shopping
@endsection
@section('content')
   <div class="row">
        <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
            <h1>Checkout</h1>
            <h4>Your total: Rs. {{$total}}</h4>
            <hr>
            <form action="/yourhome/public/checkout" method="POST" id="checkout-form">
            {{csrf_field()}}
                <div class="row">
                    <div class="col-xs-12">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control" required>
                    </div>
                     <div class="col-xs-12">
                        <label for="address">Address</label>
                        <input type="text" id="address" class="form-control" required>
                    </div>
                     <div class="col-xs-12">
                        <label for="card-name">Card holder name</label>
                        <input type="text" id="card-name" class="form-control" required>
                    </div>
                    <div class="col-xs-12">
                        <label for="card-name">Card holder name</label>
                        <input type="text" id="card-name" class="form-control" required>
                    </div>
                    <div class="col-xs-12">
                        <label for="card-number">Card number</label>
                        <input type="text" id="card-number" class="form-control" required>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="card-expire-month">Expiration month</label>
                            <input type="text" id="card-expire-month" class="form-control" required>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-xs-6">
                            <label for="card-expire-year">Expiration year</label>
                            <input type="text" id="card-expire-year" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label for="card-ccv">CCV</label>
                        <input type="text" id="card-ccv" class="form-control" required>
                    </div>
                </div>
                <hr>
                <button type="submit" class="btn btn-success">Buy now</button>
            </form>
        </div>
    </div>
@endsection