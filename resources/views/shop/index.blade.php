@extends('layouts.master')
@section('title')
    Shopping
@endsection
@section('style')
    <link rel="stylesheet" href="{{URL::to('css/product-style.css')}}"></link>
@endsection
@section('breadcrumb')
    <nav aria-label="breadcrumb" role="navigation">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Library</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data</li>
      </ol>
    </nav>
@endsection
@section('content')
    <!--<div class="row">
        <div class="col-xs-12 col-md-8">
        @foreach($products->chunk(3) as $prodChunk)
            <div class="row ">
            @foreach($prodChunk as $product)
                <div class="col-xs-4 col-sm-4">
                    <div class="thumbnail makeItClickable">
                        <img src="{{$product->imagePath}}" alt="..." class="img-responsive">
                        <div class="caption" style="text-align:center;">
                            <h3 class="productDetails">{{$product->title}}</h3>
                            <p class="description">{{$product->description}}</p>
                            <div class="clearfix">
                                <div class="price">Rs. {{$product->price}}</div>
                                <a href="/yourhome/public/add-to-cart/{{ $product->id }}" class="productDetails shopThis" role="button">Shop this</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        @endforeach
        </div>
   </div>  -->



<div class="row jumbotron">
    @foreach($products->chunk(3) as $prodChunk)  
    <div class="row">
        @foreach($prodChunk as $product)
            <div class="card thumbnail">
                <a href="/yourhome/public/check-single-product/{{ $product->id}}">
                    <img src="{{$product->imagePath}}" alt="..." class="img-responsive">
                </a>    
                <div class="caption" style="text-align:center;">
                    <h3 class="productDetails">{{$product->title}}</h3>
                <!-- <p class="description">{{$product->description}}</p>-->
                    <div class="clearfix">
                        <div class="price">Rs. {{$product->price}}</div>
                        <a href="/yourhome/public/add-to-cart/{{ $product->id }}" class="productDetails shopThis" role="button">Shop this</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @endforeach
</div>

@endsection