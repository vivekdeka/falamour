@extends('layouts.master')
@section('title')
    Shopping
@endsection
@section('style')
    <link rel="stylesheet" href="{{URL::to('css/app.css')}}"></link>
    <link rel="stylesheet" href="{{URL::to('css/shopping-cart.css')}}"></link>    
@endsection
@section('content')
<div class="row jumbotron" style="margin-top:10%;">
   <!-- @if(Session::has('cart'))
    <div class="col-md-12">
        @foreach($products as $product)
            <li class="list-group">
                <span class="badge badge-default">{{$product['qty']}}</span>
                <strong>{{$product['item']['title']}}</strong>
                <span class="label label-success">{{$product['price']}}</span>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toogle" data-toggle ="dropdown" >Action <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Reduce by 1</a></li>
                        <li><a href="#">Reduce All</a></li>
                    </ul>
                </div>
            </li>
        @endforeach
    </div>
    </div>
    
    
   @endif-->
   @if(Session::has('cart'))
   <h5>YOUR SELECTIONS</h5>
    <div class="row col-12">

        <div class="col-md-4">
            <span><strong>Product</strong></span>
        </div>
        <div class="col-md-2">
             <span><strong>Price</strong></span>
        </div>
        <div class="col-md-2">
             <span><strong>Qualtity</strong></span>
        </div>
        <div class="col-md-2">
             <span><strong>Subtotal</strong></span>
        </div>
        <div class="col-md-2">
             
        </div>
        <hr style="background: #e8edf3!important; width: 100%;">
    </div>
    
    <div class="row col-12">
        @foreach($products as $product)
            <div class="col-md-4">
                <span>{{$product['item']['title']}}</span>
            </div>
            <div class="col-md-2">
                <span class="label label-success"><i class="fa fa-inr" aria-hidden="true"></i>. {{$product['price']}}</span>
            </div>
            <div class="col-md-2">
                <span class="badge badge-default"></span>
                <select class="form-control" id="exampleSelect1">
                    <option value="{{$product['qty']}}">{{$product['qty']}}</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="col-md-2">
                <span><i class="fa fa-inr" aria-hidden="true"></i>. {{$product['subTotal']}}</span>
            </div>
            <div class="col-md-2">
                <button class="btn btn-sm" style="background:#b56969;"><i class="fa fa-trash-o"></i></button>
            </div>
        <hr>
        @endforeach
       <hr style="background: #e8edf3!important; width: 100%;"> 
    </div>
     
    <div class="row col-12">
        <div class="col-md-6" style="text-align:right;">
            <strong>Total </strong>   
        </div> 
        <div class=" col-md-6">
            <strong><i class="fa fa-inr" aria-hidden="true"></i>. {{$totalPrice}}</strong>   
        </div>   
    </div>
    <hr>
   
    <div class="row col-12">
       <a href="/yourhome/public/checkout" class="btn placeOrderBtn ">Checkout <i class="fa fa-angle-right"></i></a>
    </div>
    @else
       <div class="row">
        <div class="col-md-12">
            <h2>Shopping bag is empty.</h2>
            <div class="row col-6">
                <a href="#" class="btn continueShopping" style=""><i class="fa fa-angle-left"></i> Continue Shopping</a>
            </div>
        </div>  
    </div>         
    @endif
</div>

   
@endsection