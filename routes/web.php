<?php
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', ['uses'=>'CategoriesController@getLandingPage', 'as'=>'langingPage.index']);
Route::get('/product-list', ['uses'=>'ProductsController@showProducts', 'as'=>'shop.index']);

Route::get('add-to-cart/{id}',['uses'=>'ProductsController@getAddToCart','as'=>'product.addToCart']);
Route::get('check-single-product/{id}',['uses'=>'ProductsController@getSingleProduct','as'=>'product.checkSingleProduct']);
Route::get('shopping-cart',['uses'=>'ProductsController@getCart','as'=>'product.shoppingCart']);
Route::get('/checkout', 'ProductsController@getCheckout');
Route::post('/checkout', 'ProductsController@getCheckout');


Route::group(['middleware'=>'guest'], function(){
    Route::get('/signup', ['uses'=>'UsersController@signup']);
    Route::post('/registerUser', ['uses'=>'UsersController@create']);
    Route::get('/signin', ['uses'=>'UsersController@signin']);
    Route::post('/signin', ['uses'=>'UsersController@fnSigninUser']);
});

Route::group(['middleware'=>'auth'], function(){
    Route::get('/user/profile', ['uses'=>'UsersController@fnGetUserProfile']);
    Route::get('/logout', ['uses'=>'UsersController@fnLogout']);
});


Route::get('/about-us', ['uses'=>'MiscellaneousController@fnLoadAboutUs', 'as'=>'aboutUs.index']);


// For Admin panel
// ============================================================================================
Route::get('/admin', ['uses'=>'AdminsController@fnLoadAdminPanel', 'as'=>'adminPanel.index']);
