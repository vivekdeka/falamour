var signInApp = angular.module('signInPopup', ['ui.bootstrap']);

signInApp.controller('signInCtrl', ['$scope','$uibModal',function ($scope, $uibModal) {
   $scope.open = function () {
        console.log('opening pop up');
        var modalInstance = $uibModal.open({
            restrict: 'EA',
            replace: 'true',
            templateUrl: 'signin'
            //templateUrl: 'users/signin.blade.php',
        });
    }
}]);