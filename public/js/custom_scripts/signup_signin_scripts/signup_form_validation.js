'use strict';
var signupApp = angular.module('signupApp',['ngResource']);

signupApp.controller('signupCtrl', ['$scope','userService',function($scope, userService){
   
   $scope.userData = {};
   $scope.userData.firstName = '';
   $scope.userData.lastName = '';
   $scope.userData.email = '';
   $scope.userData.password = '';
   $scope.userData.confirmPassword = '';
   $scope.userData.phoneNumber = '';
   $scope.userData.city = '';
   $scope.userData.state = '';
   $scope.userData.zip = '';
   $scope.userData.termsAndCondition='';

   $scope.checkForSpecialCharacters = {
    regExp: /^[a-zA-Z0-9- ]*$/,
    test: function(val) {
      return !this.regExp.test(val);
    }
  };


  $scope.validateIndividualFields = function(fieldName){
      console.log(fieldName);
      $scope.showErrorMsg = false;
      $scope.message = '';
    if(fieldName == 'firstName'){
        $scope.showErrorFirstNameMsg = false;   
        if($scope.checkForSpecialCharacters.test($scope.userData.firstName) == true){
             $scope.showErrorFirstNameMsg = true;   
             $scope.message = 'First name must not contain special characters.';
        }  
    }else if(fieldName == 'lastName'){
        $scope.showErrorLastNameMsg = false;
        if($scope.checkForSpecialCharacters.test($scope.userData.lastName) == true){
            $scope.showErrorLastNameMsg = true;   
            $scope.message = 'Last name must not contain special characters.';
       }  
    }else if(fieldName== 'address'){
        $scope.showErrorAddressMsg = false;
        if($scope.checkForSpecialCharacters.test($scope.userData.address) == true){
            $scope.showErrorAddressMsg = true;   
            $scope.message = 'Address must not contain special characters.';
       }  
    }else if(fieldName== 'zip'){
        $scope.showErrorZipMsg = false;
        if($scope.checkForSpecialCharacters.test($scope.userData.zip) == true){
            $scope.showErrorZipMsg = true;   
            $scope.message = 'Zip code must not contain special characters.';
       }  
    }/*else if(fieldName== 'termsAndCondition'){
        $scope.showErrorTncMsg = false;
        if($scope.userData.termsAndCondition == 'undefined'){
            console.log('hi');
            $scope.showErrorTncMsg = true;   
            $scope.message = 'You must agree to terms and conditions';
       } 
    }*/
  }

  $scope.checkConfirmPassword = function(event){
      $scope.matchPassword = '';
      $scope.matchPasswordMsg = '';
      if($scope.userData.password.trim() == $scope.userData.confirmPassword.trim() && $scope.userData.password.trim() != "")
      {
        $scope.matchPassword = true;
      }else if($scope.userData.password.trim() != $scope.userData.confirmPassword.trim()){
          $scope.matchPassword = false;
          var passwordElement = angular.element( document.querySelector( '#password' ) );
          passwordElement.addClass('alpha');
      }
  }
    $scope.fnValidateData = function(){
        userService.registerUser('registerUser').registerUser(
        {
            data: $scope.userData
        }, function(data) {
            console.log(data);
            /*if(data.status == 'found')
            {
                $scope.currentDate= $filter('date')(new Date(data.data.startDate), 'yyyy-MM-dd');
                $scope.yesterDay= $filter('date')(data.data.endDate, 'yyyy-MM-dd');
                $scope.startDateTimeForView = new Date(data.data.startDate);
                $scope.endDateTimeForView = new Date(data.data.endDate);
            }else{
                $scope.startDateTimeForView = new Date(new Date().setDate(new Date().getDate()-1));
                $scope.startDateTimeForFilter = $filter('date')($scope.startDateTimeForView, 'yyyy-MM-dd');
            }*/
        });
    }
}]).service('userService',['$resource', function($resource){
    var apiResourceUrl = 'index.php/';
    var factory = {};
    factory.registerUser = function(queryType) {
        var queryType = arguments[0] || '';
        var registerUserRESTUri = apiResourceUrl + 'employeePerformance';
        if (queryType == 'registerUser') {
            registerUserRESTUri = apiResourceUrl + 'registerUser';
        } 
        return $resource(registerUserRESTUri, {}, {
            registerUser: {
                method: 'POST'
            }
        });
    };
    return factory;
}]);
