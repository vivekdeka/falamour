<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiscellaneousController extends Controller
{
    public function fnLoadAboutUs(){
        return view('shop.about-us');
    }
}
