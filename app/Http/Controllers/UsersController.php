<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Redirect;
use DB;
use \App\User;

class UsersController extends Controller
{
    public function signup(){
        return view('users.signup');
    }

    public function create(Request $request){
        $user = new User([
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'email' => $request->email,
             'password' => bcrypt($request->password),
             'address' => $request->address,
             'phoneNumber' => $request->phoneNumber,
             'city' => $request->city,
             'state' =>$request->state,
             'zip' =>$request->zip
            ]);
        $user->save();   
        Auth::login($user);
        //return redirect('/posts'); 
        return Redirect::to('/user/profile');
    }

    public function signin(){
        return view('users.signin');
    }

    public function fnSigninUser(Request $request){
        $this->validate($request,[
            'email'=>'email|required',
            'password'=>'required|min:6'
        ]);

        if(Auth::attempt(['email' =>$request->email,'password' => $request->password]))
        {
            return Redirect::to('/user/profile');   
        }else{
           return redirect()->back();     
        }
    }
    public function fnGetUserProfile(){
        return view('users.profile');
    }

    public function fnLogout(){
        Auth::logout();
        return redirect()->back();
    }
}
