<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use Session;
use Redirect;

class CategoriesController extends Controller
{
    public function getLandingPage(){
        $categories = Category::all();
        return view('landing-page.index', compact('categories'));
    }
    
}
