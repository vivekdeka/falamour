<?php

namespace App;

class Cart 
{
    public $items = null;
    public $totalQty = 0;
    //public $subTotal = 0;
    public $totalPrice = 0;

    public function __construct($oldCart){
        if($oldCart){
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            //$this->subTotal = $oldCart->subTotal;            
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($item, $id){
        $storedItem = [
           'qty'=>0,
           'price'=>$item->price,
           'item'=>$item ,
           'subTotal'=>0    
        ];

        if($this->items){
            if(array_key_exists($id, $this->items)){
                $this->items[$id] = $storedItem;    
            }
        }

        $storedItem['qty'] = $storedItem['qty'] + 1;
        $storedItem['price'] = $item->price;
        $storedItem['subTotal'] = $storedItem['qty'] * $item->price;    
        $this->items[$id] = $storedItem;
        $this->totalQty = $this->totalQty + 1;
        $this->totalPrice += $item->price;
    }
}
