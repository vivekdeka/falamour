<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
            'imagePath'=>'img/sample.jpg',
            'title'=>'Puma T-shirt',
            'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled',
            'price'=>12
        ]);

        $product->save();

        $product = new \App\Product([
            'imagePath'=>'img/sample.jpg',
            'title'=>'Puma T-shirt',
            'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled',
            'price'=>12
        ]);

        $product->save();

        $product = new \App\Product([
            'imagePath'=>'img/sample.jpg',
            'title'=>'Puma T-shirt',
            'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled',
            'price'=>12
        ]);

        $product->save();
    }
}
